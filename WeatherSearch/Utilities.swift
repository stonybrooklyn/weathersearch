//
//  Utilities.swift
//
//  Copyright © 2017 Shane Hartman. All rights reserved.
//

import Foundation
//this should be refactored to properly handle the string constants and parse in the query parameter delimiters if time permitted (i.e. not hardcoding them together)
struct StringConstants {
    static let searchByCityBaseURL = "http://api.openweathermap.org/data/2.5/weather?q="
    static let searchByCityIdBaseURL = "http://api.openweathermap.org/data/2.5/weather?id="
    static let searchByZipBaseURL = "http://api.openweathermap.org/data/2.5/weather?zip="
    static let searchForecastByIDBaseURL = "http://api.openweathermap.org/data/2.5/forecast?id="
    static let searchForecastByNameBaseURL = "http://api.openweathermap.org/data/2.5/forecast?q=="
    static let retrieveImageBaseURL = "http://openweathermap.org/img/w/"
    static let apiKey = "&APPID=c941259fc67adc4d14c9a446f803306d"
    static let imageFileExt = ".png"
}

struct WeatherUtils {
    static func convertKelvinToFahrenheit(_ kelvin: Double) -> Int {
        let preciseTemp: Double = ((9/5) * (kelvin - 273.15)) + 32
        let fahrenheit: Int = Int(round(preciseTemp))
        return fahrenheit
    }
    static func getURL(for requestType: RequestType, parameter: String) -> URL? {
        switch requestType {
        case .weatherWithID:
            guard let requestUrl = URL(string:StringConstants.searchByCityIdBaseURL +  parameter + StringConstants.apiKey) else {
                print("id url from string failed")
                return nil
            }
            return requestUrl
        case .weatherWithName:
            guard let requestUrl = URL(string:StringConstants.searchByCityBaseURL +  parameter + StringConstants.apiKey) else {
                print("name url from string failed")
                return nil
            }
            return requestUrl
        case .weatherWithZip:
            var zipcodeStr = ""
            if (parameter.count == 4) {
                zipcodeStr = "0" + parameter
            } else {
                zipcodeStr = parameter
            }
            guard let requestUrl = URL(string:StringConstants.searchByZipBaseURL +  zipcodeStr + StringConstants.apiKey) else {
                print("name url from string failed")
                return nil
            }
            return requestUrl
        case .forecastForID:
            guard let requestUrl = URL(string:StringConstants.searchForecastByIDBaseURL +  parameter + StringConstants.apiKey) else {
                print("forecast url from string failed")
                return nil
            }
            return requestUrl
        case .forecastForName:
            guard let requestUrl = URL(string:StringConstants.searchForecastByNameBaseURL +  parameter + StringConstants.apiKey) else {
                print("forecast url from string failed")
                return nil
            }
            return requestUrl
        case .image:
            guard let requestUrl = URL(string:StringConstants.retrieveImageBaseURL +  parameter + StringConstants.imageFileExt) else {
                print("image url from string failed")
                return nil
            }
            return requestUrl
        }
    }
    static func generateRandomZipCode() -> Int {
        let lower : UInt32 = 1000
        let upper : UInt32 = 99950
        let randomNumber = Int(arc4random_uniform(upper - lower) + lower)
        return randomNumber
    }
}
extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }

    var startOfYesterday: Date {
        let dayStart = Calendar.current.startOfDay(for: self)
        return Calendar.current.date(byAdding: .day, value: -1, to: dayStart)!
    }
    
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    
    var startOfTomorrow: Date {
        let dayStart = Calendar.current.startOfDay(for: self)
        return Calendar.current.date(byAdding: .day, value: 1, to: dayStart)!
    }
    
    var dayAfterTomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 2, to: self)!
    }
}
