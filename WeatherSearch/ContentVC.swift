//
//  ViewController.swift
//  WeatherSearch
//
//  Created by Shane Hartman on 10/11/17.
//  Copyright © 2017 Shane Hartman. All rights reserved.
//

import UIKit
import CoreData

class ContentVC: UIViewController {
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var weatherImgView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var conditionsLabel: UILabel!
    @IBOutlet var swipeView: SwipeView!
    
    var cityName: String?
    var date: String?
    var temperature: String?
    var conditions: String?
    var imgName: String?
    var imgData: Data?
    var pageWeathers: [WeatherReport] = [] 
    var titleText: String?
    var pageIndex:Int?
    var inputCity: String? {
        didSet {
            //if I had more time I would validate this data before doing API call (strength length, check for numeric characters, make sure only one comma and no other special characters, etc . . .
            performWeatherRequest(for: .weatherWithName, with: self.inputCity)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationTextField.delegate = self
        self.addBottomBorderToHeaderView()
        self.retrieveWeatherFromCoreData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func addBottomBorderToHeaderView() {
        
        let black = UIColor.black
        let bottomBorder = CALayer()
        
        bottomBorder.frame = CGRect(x: 0.0, y: self.headerView.frame.size.height, width: self.view.frame.size.width, height: 1.0)
         bottomBorder.backgroundColor = black.cgColor
        self.headerView.layer.addSublayer(bottomBorder)
        self.headerView.layoutIfNeeded()
    }
    
    func urlEncodeParameterString(forCity city:String) -> String {
        
        let escapedCityName = city.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        guard let city = escapedCityName else {
            print("could not unwrap escaped city string")
            return ""
        }
        return city
    }
    
    func parseReponseData(for type: ParseType, data: Data?) {
        switch type {
        case .weather:
            guard let data = data else {
                print("invalid or nil data")
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                guard let weatherStats = json?["main"] as? [String:Any], let temp = weatherStats["temp"] as? Double, let name = json?["name"] as? String, let id = json?["id"] as? Int else {
                    print("could not parse main json structure from serialized response")
                    return
                }
                
                guard let weatherArray = json?["weather"] as? [[String:Any]] else {
                    print("could not parse arr json structure from serialized response")
                    return
                }
                
                guard weatherArray.indices.contains(0), let description = weatherArray[0]["description"] as? String, let imgName = weatherArray[0]["icon"] as? String  else {
                    print("could not parse desc json structure from serialized response")
                    return
                }
                
                DispatchQueue.main.async { [unowned self] in
                    self.temperature = String(WeatherUtils.convertKelvinToFahrenheit(temp))
                    self.cityName = name
                    self.conditions = description
                    self.imgName = imgName
                    self.save(id:id)
                    self.updateUI()
                }
                
            } catch let error as NSError {
                print(error)
            }
        case .forecast:
            guard let data = data else {
                print("invalid or nil data")
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                guard let weatherArray = json?["list"] as? [[String:Any]?] else {
                    print("could not parse forecst arr json structure from serialized response")
                    return
                }
                let reversedForecast = Array(weatherArray.reversed())
                guard reversedForecast.indices.contains(3), let day5 = reversedForecast[3] as? [String:Any], reversedForecast.indices.contains(11), let day4 = reversedForecast[11] as? [String:Any], reversedForecast.indices.contains(19), let day3 = reversedForecast[19] as? [String:Any], reversedForecast.indices.contains(27), let day2 = reversedForecast[27] as? [String:Any], reversedForecast.indices.contains(35), let day1 = reversedForecast[35] as? [String:Any] else {
                    print("could not parse forecast days from serialized response")
                    return
                }
                self.extractWeatherFromJSONArray(forecast: day1)
                self.extractWeatherFromJSONArray(forecast: day2)
                self.extractWeatherFromJSONArray(forecast: day3)
                self.extractWeatherFromJSONArray(forecast: day4)
                self.extractWeatherFromJSONArray(forecast: day5)
            } catch let error as NSError {
                print(error)
            }
        }
    }
    //this should be reactored to properly handle the string constants and parse in the query parameter delimiters if time permitted i.e. not hardcoding them together)
    func performWeatherRequest(for type: RequestType, with parameter:String?) {
        guard let queryParam = parameter else {
            print("paramater is nil")
            return
        }
        guard let requestUrl = WeatherUtils.getURL(for: type, parameter: self.urlEncodeParameterString(forCity: queryParam)) else {
            print("url from string failed")
            return
        }

        switch type {
        case .weatherWithZip:
            let request = URLRequest(url:requestUrl)
            let task = URLSession.shared.dataTask(with: request) {
                (data, response, error) in
                guard let httpResponse = response as? HTTPURLResponse else {
                    print("could not downcast to httpresponse")
                    return
                }
                if (httpResponse.statusCode == 404) {
                    self.performWeatherRequest(for: .weatherWithZip, with: String(WeatherUtils.generateRandomZipCode()))
                }
                if error == nil,let usableData = data {
                    self.parseReponseData(for: .weather, data: usableData)
                }
                else {
                    print(error.debugDescription)
                }
            }
            task.resume()
        default:
            let request = URLRequest(url:requestUrl)
            let task = URLSession.shared.dataTask(with: request) {
                (data, response, error) in
                print(error.debugDescription)
                if error == nil,let usableData = data {
                    switch type {
                    case .forecastForID:
                        self.parseReponseData(for: .forecast, data: usableData)
                    case .forecastForName:
                        self.parseReponseData(for: .forecast, data: usableData)
                    default:
                        self.parseReponseData(for: .weather, data: usableData)
                    }
                }
                else {
                    print(error.debugDescription)
                }
            }
            task.resume()
        }
    }
    
    func retrieveWeatherFromCoreData() {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        
        let request =
            NSFetchRequest<NSManagedObject>(entityName: "Weather")
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        request.fetchLimit = 1
        
        var fetchResults: [NSManagedObject] = []
        do {
            fetchResults = try managedContext.fetch(request)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        guard let weather = fetchResults.first, let code = weather.value(forKeyPath: "cityCode") as? Int, let name = weather.value(forKeyPath: "cityName") as? String  else {
            print("no weather is available for today")
            return
        }
        //account for missing city code in json response data for api queries by zipcode
        if (code > 0) {
            self.performWeatherRequest(for: .weatherWithID, with: String(code))
        }
        else {
            self.performWeatherRequest(for: .weatherWithName, with: name)
        }

    }
    
    func save(id: Int) {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        
        let entity =
            NSEntityDescription.entity(forEntityName: "Weather",
                                       in: managedContext)!
        
        let weather = NSManagedObject(entity: entity,
                                      insertInto: managedContext)
        
        weather.setValue(self.conditions, forKeyPath: "conditions")
        weather.setValue(self.temperature, forKeyPath: "temperature")
        weather.setValue(self.cityName, forKeyPath: "cityName")
        weather.setValue(self.imgName, forKeyPath: "imgName")
        weather.setValue(id, forKeyPath: "cityCode")
        weather.setValue(Date(), forKey: "date")
        let weatherReport = WeatherReport(date: Date(), conditions: self.conditions, imgName: self.imgName, temp: self.temperature)
        guard let report = weatherReport else {
            print("could not access report")
            return
        }
        report.city = self.cityName
        self.pageWeathers.append(report)
        print("save called")
        if (id > 0) {
            self.performWeatherRequest(for: .forecastForID, with: String(id))
        }
        else {
            self.performWeatherRequest(for: .forecastForName, with: self.cityName)
        }
        do {
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    func updateUI() {
        guard  let temp = self.temperature else {
            print("temp is nil")
            return
        }
        self.titleLabel.attributedText = self.getDayAndPlaceText(for: self.cityName)
        self.tempLabel?.textColor = UIColor.black
        self.tempLabel?.text = temp + " º F"
        self.conditionsLabel.textColor = UIColor.black
        self.conditionsLabel.text = self.conditions
        guard let imageCode = self.imgName else {
            print("imgName is nil")
            return
        }
        guard let url = WeatherUtils.getURL(for: .image, parameter: imageCode) else {
            print("imgName is nil")
            return
        }
        self.weatherImgView.setImageFromURl(stringImageUrl: url)
    }
    
    func getDayAndPlaceText(for place: String?) -> NSMutableAttributedString {
        guard let name = self.cityName else {
            print("could not get city name")
            return NSMutableAttributedString(string: "")
        }
        let baseText = "Today"
        let text = baseText + " in " + name
        
        var labelText: NSMutableAttributedString = NSMutableAttributedString(string: text)
        labelText.addAttributes([NSAttributedStringKey.foregroundColor: UIColor.blue], range: NSMakeRange(0, baseText.count))
        labelText.addAttributes([NSAttributedStringKey.foregroundColor: UIColor.black], range: NSMakeRange(baseText.count, name.count + 4))
        return labelText
    }
    
    func extractWeatherFromJSONArray(forecast: [String: Any]) {
        
        guard let weatherStats = forecast["main"] as? [String:Any], let temp = weatherStats["temp"] as? Double, let date = forecast["dt_txt"] as? String  else {
            print("could not parse main json structure from serialized response")
            return
        }
        
        guard let weatherArray = forecast["weather"] as? [[String:Any]] else {
            print("could not parse arr json structure from serialized response")
            return
        }
        //make sure index is within bounds
        let weatherArrIdx = 0
        let isIndexValid = weatherArray.indices.contains(weatherArrIdx)
        if (!isIndexValid) {
            print("array does not contain index")
            return
        }
        guard let description = weatherArray[weatherArrIdx]["description"] as? String, let imgName = weatherArray[weatherArrIdx]["icon"] as? String  else {
            print("could not parse desc json structure from serialized response")
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm:ss +zzzz"
        dateFormatter.locale = Locale.init(identifier: "en_US")
        let dateObj = dateFormatter.date(from: date)
        let weatherReport = WeatherReport(date: dateObj, conditions: description, imgName: imgName, temp: String(WeatherUtils.convertKelvinToFahrenheit(temp)))
        guard let report = weatherReport else {
            print("could not access report")
            return
        }
        self.pageWeathers.append(report)
        
    }
    
    @IBAction func randomWeatherBtnTapped(_ sender: UIButton) {
        self.performWeatherRequest(for: .weatherWithZip, with: String(WeatherUtils.generateRandomZipCode()))
    }
    
}

extension ContentVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        self.locationTextField.placeholder = nil
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.inputCity = self.locationTextField.text
        self.locationTextField.text = nil
        self.locationTextField.placeholder = "Enter Your City"
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
}

extension UIImageView{
    func setImageFromURl(stringImageUrl url: URL){
            if let data = NSData(contentsOf: url) {
                self.image = UIImage(data: data as Data)
        }
    }
}

enum RequestType {
    case weatherWithName
    case weatherWithID
    case weatherWithZip
    case forecastForID
    case forecastForName
    case image
}

enum ParseType {
    case weather
    case forecast
}
//if I had more time I would abstract the API service call logic out into a discrete class
/*struct WeatherServiceManager {
 
 func performSearchWithCityId(for cityID: Int) -> Data? {
 
 }
 
 func performSearchWithCityName(for cityName: String) -> Data? {
 
 }
 //if I had more time I would add data validation for date parameter to make sure it is in the future as this method is designed to get the forecast
 func performSearchWithCityId(for cityID: Int, for date: String) -> Data? {
 
 }
 
 }*/

