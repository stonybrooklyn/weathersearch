//
//  WeatherReport.swift
//  WeatherSearch
//
//  Created by Shane Hartman on 10/16/17.
//  Copyright © 2017 Shane Hartman. All rights reserved.
//

import Foundation

class WeatherReport {
    var date: Date?
    var city: String?
    var conditions: String?
    var imgName: String?
    var temp: String?
    
    convenience init?(date: Date?, conditions: String?, imgName: String?, temp: String?) {
        self.init()
        self.date = date
        self.conditions = conditions
        self.imgName = imgName
        self.temp = temp
    }
}
