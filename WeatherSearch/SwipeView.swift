//
//  SwipeView.swift
//  WeatherSearch
//
//  Created by Shane Hartman on 10/16/17.
//  Copyright © 2017 Shane Hartman. All rights reserved.
//

import Foundation
import UIKit

class SwipeView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var weatherImgView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var conditionsLabel: UILabel!
    
    func animateTitleLabel() {
        
        self.layoutIfNeeded()
    }
}
